from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *
from .models import Status
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest

# Create your tests here.
class StatusTest(TestCase):
	def test_steven_url_is_exist(self):
		response = Client().get('')
		self.assertEqual(response.status_code,200)
	
	def test_story6_using_lab6_template(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'steven.html')
	
	def test_model_create_new_status(self):
		new_review = Status.objects.create(date=timezone.now(), status='Coba coba')
		counting_review_object = Status.objects.all().count()
		self.assertEqual(counting_review_object, 1)

class FunctionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--disable-dev-shm-usage')
		chrome_options.add_argument('--disable-gpu')
		chrome_options.add_argument('--dns-prefetch-disable')
		self.browser = webdriver.Chrome(chrome_options=chrome_options)
		super().setUp()
	
	def tearDown(self): 	
		self.browser.quit()
		super().tearDown()
	
	# Fungsi ini mengecek apabila karakter lebih dari 300
	# Contoh : Bila karakter berjumlah 400, yang masuk adalah 300 karakter pertamanya
	def test_if_char_more_than_300(self):
		self.browser.get(self.live_server_url)
		msg = self.browser.find_element_by_id('status')
		send = self.browser.find_element_by_id('submit')
		long_status = ""
		for i in range(401):
			long_status = long_status + str(i%10)
		msg.send_keys(long_status)
		send.send_keys(Keys.RETURN)
		results = self.browser.find_elements_by_class_name('mb-1')[0].text
		self.assertNotIn(long_status, results)
	
	# Fungsi ini mengecek apakah status yang disubmit muncul di beranda
	def test_input_status(self):
		self.browser.get(self.live_server_url)
		msg = self.browser.find_element_by_id('status')
		send = self.browser.find_element_by_id('submit')
		msg.send_keys('Coba coba')
		send.send_keys(Keys.RETURN)
		results = self.browser.find_elements_by_class_name('mb-1')[0].text
		self.assertIn('Coba coba', results)
		