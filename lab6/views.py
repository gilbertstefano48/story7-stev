from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import StatusForm
from .models import Status

# Create your views here.
def status(request):
	status = Status.objects.order_by('date')
	form = StatusForm(request.POST)
	content = {'form' : form, 'status': status}
	return render(request, 'steven.html', content)

def save_status(request):
	if request.method == 'POST':
		form = StatusForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('lab6:status')
	else:
		return redirect('lab6:status')
