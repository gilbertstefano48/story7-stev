from django.db import models
import datetime

# Create your models here.
class Status(models.Model):
	date = models.DateTimeField(auto_now_add=True)
	status = models.TextField(max_length=300)