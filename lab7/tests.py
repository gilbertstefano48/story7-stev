from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest

# Create your tests here.
class Story7Test(TestCase):
	def test_steven_url_is_exist(self):
		response = Client().get('/story7')
		self.assertEqual(response.status_code,200)
	
	def test_story6_using_lab7_template(self):
		response = Client().get('/story7')
		self.assertTemplateUsed(response, 'lab7.html')