from .views import *
from django.shortcuts import render
from django.urls import path

app_name = 'lab7'
urlpatterns = [
    path('', lab7View, name='lab7'),
]
